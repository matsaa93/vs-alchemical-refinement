#!/bin/sh
rm -rf build/*
mkdir build /tmp/mods
cp -r resources/* build/
cp modinfo.json build/modinfo.json
cd build
zip -r alchemical-refinement-rc.zip assets/* modinfo.json
mv alchemical-refinement-rc.zip ../alchemical-refinement-rc.zip
