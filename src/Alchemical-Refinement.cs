using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Server;

[assembly: ModInfo( "HowtoExample",
	Description = "A mod that lets you refine ores/metals following the Alchemical Process",
	Website     = "https://gitlab.com/matsaa93/vs-alchemical-refinement",
	Authors     = new []{ "matsaa93" } )]

namespace AlchemicalRefinement
{
    public class AlchemicalRefinementMod : ModSystem
    {
        public override void Start(ICoreAPI api)
        {
        
        }

        public override void StartClientSide(ICoreClientAPI api)
		{
			
		}
		
		public override void StartServerSide(ICoreServerAPI api)
		{
			
		}
    }
    
}