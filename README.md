# Alchemical Refinement

## Description
A Vintage Story mod that lets you refine metals following the alchemical process.

## Usage
Linux: 
```shell
./build.sh && mv alchemical-refinement-rc.zip ${HOME}/.config/VintagestoryData/Mods/
```

## Support
I dont know the internet, for now.

## Roadmap
adding alchemical apparatuses
cuppeling mechanic (bismuth and lead collector metal)
(Al)chemical reaction crafting mechanic
add otherwise forgotten materials


## Contributing
For people who want to help or has any ideas for future mechanics and suggestion, open a issue it's the easiest way for me to remember your suggestion.
please report bugs if any is found, even the exploity ones.

## License
GNU GENERAL PUBLIC LICENSE version 2 (GPLV2)

## Project status
Extreamly Alpha
