#!/bin/bash

VINTAGE_PATH="/usr/share/vintagestory/"
MOD_PATH="/tmp/mods/"
VINTAGE_STORY_DATA="/tmp/VintagestoryData"
./build.sh
mv alchemical-refinement-rc.zip /tmp/mods/alchemical-refinement-rc.zip
cd ${VINTAGE_PATH}
mono Vintagestory.exe -oTestworld -pcreativebuilding --addModPath="${MOD_PATH}"
